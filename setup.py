import setuptools

setuptools.setup(
    name="circumference",
    version="0.0.1",
    author="Matt Tichenor",
    description="A package for calculating circle circumference",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)